﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallScaler : MonoBehaviour
{
    public LayerMask layermask;

    bool _isScaling = false;
    PlayerController controller;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position + (Vector3.up * 0.08f), 1.1f);
    }

    IEnumerator ScaleWall(float verticalMomentum)
    {
        do
        {
            controller.playerVelocity += new Vector3(0, verticalMomentum, 0) * Time.deltaTime;
            verticalMomentum -= Time.deltaTime * controller.playerVelocity.y * 3;
            yield return new WaitForEndOfFrame();
        } while (_isScaling && Input.GetKey(KeyCode.Space) == true && controller.playerVelocity.y > 0 && Physics.OverlapSphere(transform.position + (Vector3.up * 0.08f), 1.1f, layermask).Length > 0);


            _isScaling = false;
            controller.gravityScale = 1;

        
    }

    IEnumerator WallRun(float verticalMomentum)
    {

        do
        {
            controller.playerVelocity += new Vector3(0, verticalMomentum, 0) * Time.deltaTime;
            verticalMomentum -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        } while (_isScaling && Input.GetKey(KeyCode.Space) == true|| verticalMomentum > 0 || Physics.OverlapSphere(transform.position + (Vector3.up * 0.08f), 1.1f, layermask).Length > 0);


        _isScaling = false;
        controller.gravityScale = 1;


    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (Vector3.Angle(hit.normal, Vector3.up) < 25)
        {
            _isScaling = false;
            controller.gravityScale = 1;
        }

        if (_isScaling)
            return;

        if (Input.GetKey(KeyCode.Space)) {

            if (RunningInto(hit) && IsVerticalWall(hit) && LookingAt(hit))
            {
                StartCoroutine(ScaleWall(5 + controller.playerVelocity.y));
                _isScaling = true;
                controller.gravityScale = 0.5f;
            } else if (Vector3.Angle(hit.normal, Vector3.up) > 75)
            {
                StartCoroutine(WallRun(1));
                _isScaling = true;
                controller.gravityScale = 0.5f;
            }

        }
    }

    bool IsVerticalWall(ControllerColliderHit hit)
    {
        return Vector3.Angle(hit.normal, Vector3.up) > 60;
    }

    bool RunningInto(ControllerColliderHit hit)
    {
        Vector3 tempVect = new Vector3(controller.playerVelocity.x, 0, controller.playerVelocity.z);
        return Vector3.Angle(hit.normal, -tempVect) < 15;
    }


    bool LookingAt(ControllerColliderHit hit)
    {
        return Vector3.Angle(-transform.forward, hit.normal) < 15;
    }

}
