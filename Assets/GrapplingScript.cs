﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplingScript : MonoBehaviour
{
    private bool fired;
    public GameObject hook;
    public bool hooked = false;
    public float hookTravelSpeed;
    private float currentDistance;
    public float hookMaxDistance;
    public float minAccelerationtetherLength;
    public float tetherLength;
    public float hookReelSpeed;
    public float bounceAvoidanceAngle;
    public float maxHookSpeed;
    public GameObject HookHolder;
    public GameObject projectile;
    Rigidbody hookRB;
    Rigidbody rb;
    public AudioSource audio;
    PlayerController controller;
    public GameObject cam;

    // Start is called before the first frame update
    void Start()
    {
        hookRB = hook.GetComponent<Rigidbody>();
        rb = GetComponent<Rigidbody>();
        controller = GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonUp(0) && fired){
            ReturnHook();
        }

        if (Input.GetMouseButtonDown(2))
        {
            Shoot();
        }

        if (Input.GetMouseButtonDown(0) && fired == false)
        {
            //audio.pitch = UnityEngine.Random.RandomRange(0.1f, 1.9f);
            audio.Play();
            hook.transform.parent = null;
            hookRB.isKinematic = false;
            hookRB.velocity = Vector3.zero;
            hookRB.AddForce(cam.transform.TransformDirection(Vector3.forward) 
                * hookTravelSpeed 
                + (cam.transform.TransformDirection(Vector3.forward) *
                Vector3.Dot(cam.transform.TransformDirection(Vector3.forward), controller.playerVelocity)), 
                ForceMode.Impulse);
            fired = true;
        }

        if (fired && hooked == false)
        {

            currentDistance = Vector3.Distance(transform.position, hook.transform.position);

            if (currentDistance > hookMaxDistance)
                ReturnHook();
        }

        if (hooked)
        {
            Swing();

            if(Input.GetMouseButton(1)){
                ReelIn();
            }
            
            //GetComponent<Rigidbody>().AddForce((hook.transform.position - transform.position) * hookReelSpeed * Time.deltaTime, ForceMode.Force);
            //float distanceToHook = Vector3.Distance(transform.position, hook.transform.position);
            //if (distanceToHook < 1)
            //    ReturnHook();
        }
    }

    /// <summary>
    /// Restricts player movement to that of a tethered swing. should run on update
    /// </summary>
    private void Swing()
    {
        //The position we are going to check if is valid
        Vector3 testPosition = transform.position + controller.playerVelocity * Time.deltaTime;

        //If the testposition is further away, than the length of our tether
        if (Vector3.Distance(testPosition, hook.transform.position) > tetherLength)
        {
            //Pull in the testposition, so that it is within the range of the tether
            testPosition = hook.transform.position + (testPosition - hook.transform.position).normalized * tetherLength;

            //Calculate the angle of the "pull in", this is used to figure out if we should come to a complete stop,
            //As if we fall directly down, and is caught by the tether
            float changeAngle = Vector3.Angle((testPosition - transform.position).normalized, controller.playerVelocity.normalized);

            //If the angle is low, we use the angle to update our velocity
            if (changeAngle < 90)
            {
                controller.playerVelocity = (testPosition - transform.position).normalized * controller.playerVelocity.magnitude;
            }
            //If the angle is large, we kill our velocity
            else
            {
                float velocityPercent = (180 - changeAngle) * 0.01f;
                controller.playerVelocity = (testPosition - transform.position).normalized * controller.playerVelocity.magnitude * velocityPercent;
            }


            //Update the player position to the testposition we modified
            transform.position = testPosition;

        }
    }

    private void Shoot()
    {
        GameObject projectileInstance = Instantiate(projectile, transform.position, Quaternion.identity);
        projectileInstance.transform.parent = null;

        Rigidbody projectileRB = projectileInstance.GetComponent<Rigidbody>();


        projectileRB.isKinematic = false;
        projectileRB.velocity = Vector3.zero;
        projectileRB.AddForce(cam.transform.TransformDirection(Vector3.forward)
            * 1000
            + (cam.transform.TransformDirection(Vector3.forward) *
            Vector3.Dot(cam.transform.TransformDirection(Vector3.forward), controller.playerVelocity)),
            ForceMode.Impulse);
    }






    private void ReelIn()
    {
        Vector3 wishDir = (hook.transform.position - transform.position);
        wishDir.Normalize();

        float currentSpeed = Vector3.Dot(wishDir, controller.playerVelocity);

        if (currentSpeed > maxHookSpeed)
            return;


        if (tetherLength > minAccelerationtetherLength)
        {
            controller.playerVelocity -= 0.075f * controller.playerVelocity * controller.playerVelocity.magnitude * Time.deltaTime;
            controller.playerVelocity += (hook.transform.position - transform.position).normalized * Time.deltaTime * hookReelSpeed;
        }


        if (tetherLength > Vector3.Distance(hook.transform.position, transform.position))
        {
            tetherLength = Vector3.Distance(hook.transform.position, transform.position);
        }
    }

    void OnGUI()
    {
        GUI.Box(new Rect(Screen.width / 2, Screen.height / 2, 10, 10), "");
    }


    private void ReturnHook()
    {
        controller.gravityScale = 1;
        hooked = false;
        hook.transform.parent = cam.transform;
        hookRB.isKinematic = true;
        hook.transform.position = HookHolder.transform.position;
        fired = false;
    }
}
