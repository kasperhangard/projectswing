﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookDetector : MonoBehaviour
{

    public GameObject player;
    GrapplingScript grapple;
    Rigidbody rb;
    LineRenderer line;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        grapple = player.GetComponent<GrapplingScript>();
        rb = GetComponent<Rigidbody>();
        line = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        line.SetPositions(new Vector3[] { transform.position, player.transform.position });
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player.gameObject)
            return;

        if (player.GetComponent<GrapplingScript>().hooked)
            return;

        
        grapple.hooked = true;
        grapple.tetherLength = Vector3.Distance(transform.position, player.transform.position);
        rb.isKinematic = true;
        rb.velocity = Vector3.zero;
    }
    

}
