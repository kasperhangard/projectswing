﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTeleporter : MonoBehaviour
{
    public List<Transform> LevelTeleports;
    int index = 0;
    CharacterController controller;

    public void TeleportCurrent()
    {
        controller.enabled = false;

        transform.position = LevelTeleports[index].position;

        controller.enabled = true;
    }

    public void TeleportNext()
    {
        controller.enabled = false;

        transform.position = LevelTeleports[++index].position;

        controller.enabled = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnControllerColliderHit(ControllerColliderHit hit)
    {

        if (hit.transform.GetComponent<TeleportCollider>() != null)
        {
            if (hit.transform.GetComponent<TeleportCollider>().Finish)
            {
                TeleportNext();
            }
            else
            {
                TeleportCurrent();
            }
        }



        Transform current = hit.transform;
        while(current.parent != null)
        {
            if (current.GetComponent<TeleportCollider>() != null)
            {
                if (current.GetComponent<TeleportCollider>().Finish)
                {
                    TeleportNext();
                }
                else
                {
                    TeleportCurrent();
                }
                break;
            }
            current = current.parent;
        }

        Debug.Log(current.name);

    }
}
